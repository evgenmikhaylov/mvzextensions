Pod::Spec.new do |s|

s.name                = "MVZExtensions"
s.version             = "0.0.1"
s.summary             = "iOS useful classes"
s.homepage            = "https://evgenmikhaylov@bitbucket.org/evgenmikhaylov/mvzextensions"
s.license             = 'MIT'
s.author              = { "Evgeny Mikhaylov" => "evgenmikhaylov@gmail.com" }
s.source              = { :git => "https://evgenmikhaylov@bitbucket.org/evgenmikhaylov/mvzextensions.git", :tag => "0.0.1" }
s.platform            = :ios, '8.0'
s.requires_arc        = true
s.source_files        = 'MVZExtensions/*.{h,m}'

s.subspec 'Categories' do |ss|
ss.source_files       = 'MVZExtensions/Categories/*.{h,m}'
end

s.dependency            'ReactiveCocoa', '~> 2.5'

end
